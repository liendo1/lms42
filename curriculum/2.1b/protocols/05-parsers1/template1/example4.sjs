/*
 * Variable references and function calls.
 *
 * Note that both of these are *expressions*, in that they result in a value,
 * and can all be used anywhere you could also use a literal string or number.
 * Some examples of expressions:
 *  - 42
 *  - "Some string"
 *  - someVariable
 *  - input("What's your name?")
 */

// Call the print function with a single argument. The function call is an expression,
// which we're using directly as a statement. (Like the `42;` earlier.)
print("Welcome!"); 

// Functions call also be make without arguments.
print(); 

// Here, the function call expression is used as the value to initialize a variable with.
var name = input("Your name (or leave empty for default): ");
if (name == "") {
    name = "world";
} else {
    // Do nothing when name is not an empty string
}

// Function call with multiple arguments
print("Hello", name, "!"); 
