- |
    In this assignment we will make an application that stores and retrieves books from and to a file. The challenge is to read the collection of books stored in the file and process it to a practical data structure so that we can search through the collection.

    *Note:* You will need to create the `.py` file yourself this time.
-   Features:
    -   Make a menu:
        -
            text: |
                The application asks the user for a file name and checks if the file exists.
                ```
                *** Welcome to the book collection ***
                Enter the name of the file containing your books (type 'exit' to quit the application): hacker_books
                File does not exist. Please try again (type 'exit' to quit the application): hacker_books.catalog

                ```
                - The application asks the user to input a file name.
                - The application checks if the file exists. If not, it says so and asks again.
                - The application quits the application if the user inputs 'exit'.
            ^merge: feature
        
    -   Process file input:
        -
            text: |
                Once the file contents have been read the application should start processing the contents of the file. The file is structured as follows: 

                1. The first line in the file contains a description of all the elements of a book. All subsequent lines in the file are books.
                2. In all the lines in the file the elements are enclosed by '<' and '>' characters.
                Store all book entries in proper variables for further processing.
                ```
                *** Welcome to the book collection ***
                Enter the name of the file containing your books (type 'exit' to quit the application): hacker_books
                File does not exist. Please try again (type 'exit' to quit the application): hacker_books.catalog
                Reading contents....
                Read 100 books.
                Book with the highest rating (4.97) is 'Hacking Growth'.
                The lowest rating is 4.12.
                The average rating is 4.5.

                What do you want to do next?
                1. Search through the catalog
                2. Read another file
                Please enter your choice: _
                ```
                - The application should compute the lowest and average rating.
                - The application checks whether a valid input has been supplied.
                - If the user selects option 2 (Read another file) the application should ask the user to provide a file name. Before the application reads and processes the contents of this file it should clear its internal list of previously imported books.  
                - The application quits the application when the user inputs 'exit'. Note that this should be case insensitive (so 'Exit' also exits the application).

                *Hints* on how you may want to do this:
                - Open the file and read it line by line.
                - Remove the first and last character from the line, and split the remaining part on `><`, in order to get a list of values.
                - The values from the first line are actually the headers. All other lines represent actual books.
                - Combine the header values with the values for a book to create a dictionary for this book. For instance: `{"ranking": "12", "title": "Linux Bible", etc...}`.
                - Create a list of book dictionaries. This is a convenient way to represent your data, making it easy for the rest of your program to work with.
            ^merge: feature
        
    
    -   Search through the catalog:
        -
            text: |
                
                ```
                *** Welcome to the book collection ***
                Enter the name of the file containing your books (type 'exit' to quit the application): hacker_books
                File does not exist. Please try again (type 'exit' to quit the application): hacker_books.catalog
                Reading contents....
                Read 100 books.
                Book with the highest rating (4.97) is 'Hacking Growth'.
                The lowest rating is 4.12.
                The average rating is 4.5.

                What do you want to do next?
                1. Search through the catalog
                2. Read another file
                Please enter your choice: 1
                Search keyword: Linux
                Found 12 books.

                Result set: 1 - 5
                1. Hacking University
                2. The Basics of Hacking and Penetration Testing, Second Edition
                3. Linux
                4. Hacking Linux Exposed, Second Edition
                5. Kali Linux

                What do you want to do next?
                1. Search through the catalog
                2. Read another file
                ```
                - The application asks the user to input a search keyword.
                - The application checks whether a valid input has been supplied.
                - The application searches through the summaries of the books.
            ^merge: feature
        
    -   Paging the results:
        -
            text: |
                ```
                *** Welcome to the book collection ***
                Enter the name of the file containing your books (type 'exit' to quit the application): hacker_books
                File does not exist. Please try again (type 'exit' to quit the application): hacker_books.catalog
                Reading contents....
                Read 100 books.
                Book with the highest rating (4.97) is 'Hacking Growth'.
                The lowest rating is 4.12.
                The average rating is 4.5.

                What do you want to do next?
                1. Search through the catalog
                2. Read another file
                Please enter your choice: 1
                Search keyword: Linux
                Found 12 books.

                Result set: 1 - 5
                1. Hacking University
                2. The Basics of Hacking and Penetration Testing, Second Edition
                3. Linux
                4. Hacking Linux Exposed, Second Edition
                5. Kali Linux

                What do you want to do next?
                1. Previous 5 results
                2. Next 5 results
                0. Exit paging
                Please enter your choice: 2

                Result set: 6 - 10
                6 Programming Linux Hacker Tools Uncovered
                7 Basic Security Testing with Kali Linux 2
                8 Kali Linux Wireless Penetration Testing
                9 Penetration Testing with Raspberry Pi
                10 Mastering Kali Linux for Advanced Penetration Testing

                What do you want to do next?
                1. Previous 5 results
                2. Next 5 results
                0. Exit paging
                Please enter your choice: 2

                Result set: 11 - 12
                11 Hacking
                12 Web Penetration Testing with Kali Linux

                What do you want to do next?
                1. Previous 5 results
                2. Next 5 results
                0. Exit paging
                ```
                - The application allows the user to navigate through the paged results.
            ^merge: feature
            
    -   Export the results:
        -
            text: |
                ```
                *** Welcome to the book collection ***
                Enter the name of the file containing your books (type 'exit' to quit the application): hacker_books
                File does not exist. Please try again (type 'exit' to quit the application): hacker_books.catalog
                Reading contents....
                Read 100 books.
                Book with the highest rating (4.97) is 'Hacking Growth'.
                The lowest rating is 4.12.
                The average rating is 4.5.

                What do you want to do next?
                1. Search through the catalog
                2. Read another file
                Please enter your choice: 1
                Search keyword: Linux
                Found 12 books.

                Result set: 1 - 5
                1. Hacking University
                2. The Basics of Hacking and Penetration Testing, Second Edition
                3. Linux
                4. Hacking Linux Exposed, Second Edition
                5. Kali Linux

                What do you want to do next?
                1. Previous 5 results
                2. Next 5 results
                3. Export results
                0. Exit paging
                ```
                - The application allows the user to export the results to a file in the same format as .catalog file.
                - Note that the user should be able to read the exported file in this application. In this case the application should only show the books that are stored in the file and the user should be able to search these. Other functionality like paging and exporting the filtered list should also still work.
            ^merge: feature
        
            
            