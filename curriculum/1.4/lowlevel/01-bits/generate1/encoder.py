from PIL import Image
import sys
import os
from bit_writer import BitWriter

def encode(image):
    bs = BitWriter()

    bs.write_number(image.width, 16)
    bs.write_number(image.height, 16)

    image = image.convert("RGB")
    pixels = image.tobytes()
    last = [-255, -255, -255]

    for pos in range(0, len(pixels), 3):
        current = pixels[pos:pos+3]
        if current == last:
            bs.write_bit(1) # Pixel identical to previous
        else:
            bs.write_bit(0) # Pixel not identical to previous
            for channel in range(3):
                delta = current[channel] - last[channel]
                if delta<=3 and delta>=-4:
                    bs.write_bit(1) # Use delta
                    bs.write_number(delta, 3, True)
                else:
                    bs.write_bit(0) # Use absolute
                    bs.write_number(current[channel], 8)
        last = current

    print(f"Compressed from {len(pixels)} to {len(bs.bytes)}")
    
    return b"Img42" + bs.bytes

if __name__ == "__main__":
    in_name = sys.argv[1]
    out_name = os.path.splitext(in_name)[0] + '.img42'
    with open(sys.argv[1],'rb') as in_file:
        image = Image.open(in_file,'r')
        result = encode(image)
        with open(out_name,'wb') as out_file:
            out_file.write(result)
