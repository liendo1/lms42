name: Hello Rust
goals:
    rust-basics: 1
days: 2
allow_longer: true
assignment:
    Welcome to rust:
        - |
            During the rest of this module, we'll be learning a low-level programming language called Rust. (Don't worry, although it's low-level it's a *lot* easier to work with than SAX16 assembly.) Rust is a relatively new language, originally conceived for improving Firefox, that is gaining popularity very rapidly. It is meant as a better replacement for the *classic* low-level programming languages, like C (1972) and C++ (1985).
        -
            link: https://www.youtube.com/watch?v=lKTo7aDf0bE
            title: Rust in 2018 - A Fast Tour
            info: This video gives you a quick look at Rust and some of the things that make it a great language.
        -
            link: https://www.rerun.io/blog/why-rust
            title: Emil Ernerfeldt - Why Rust? 
            info: "If you need more convincing: this is a blog article from a seasoned low-level programmer, explaining why he's excited about Rust. Don't worry about the specifics mentioned in the later sections - we'll get to (most of) them in due course."
        - Install `rustup`, a tool that manages installation of all necessary Rust components, using Manjaro's `Add/remove programs`.
        - While we're at it, let's also install the `rust-analyzer` extension by *rust-lang* in Visual Studio Code.

    The basics:
        - |
            The 'official' Rust Book is considered the best resource for learning Rust, and it's available online for free! We'll be following this book pretty closely during the rest of this module.

            There'll be quite a lot of reading to do. But make sure you try to follow along with the examples, or to do your own experiments on the subjects being explained. If you just read through the chapters without practicing, you'll probably discover that you don't know what to do when you get to the end of the chapters.
        -
            link: https://doc.rust-lang.org/stable/book/ch01-00-getting-started.html
            title: The Rust Book - Getting started
            info: You can skip the *installing rustup* part of course, since we already did that.
        -
            link: https://doc.rust-lang.org/stable/book/ch02-00-guessing-game-tutorial.html
            title: The Rust Book - Programming a Guessing Game
            info: Run the example. Does everything make sense? Do changes you make work the way you imagined?
        -
            link: https://doc.rust-lang.org/stable/book/ch03-00-common-programming-concepts.html
            title: The Rust Book - Common Programming Concepts
            info: Don't forget to experiment!

    Ownership:
        - Up til this point, we've learned a lot of things about Rust that are *different* from Python, but mostly superficially. We're now getting into *ownership*, which is really the thing that makes Rust a special and well-regarded language.
        -
            link: https://doc.rust-lang.org/book/ch04-00-understanding-ownership.html
            title: The Rust Book - Understanding Ownership
            info: |
                This chapter will talk a lot about some of the concepts we've encountered on an even lower level using SAX16 assembly: (address) references and the call stack. Besides the stack as a way to store that, it also introduces *the heap* (not to be confused with the *min/max heap data structure*, which is unrelated).

                Make an effort to *really* understand what's being explained here. If you're struggling, watch the next video, and then come back to read this text again.

        -
            link: https://www.youtube.com/watch?v=lQ7XF-6HYGc&list=PLLqEtX6ql2EyPAZ1M2_C0GgVd4A-_L4_5&index=13
            title: Doug Milford - Rust Ownership and Borrowing
            info: A video explaining ownership. We recommend watching this video fully, and then rereading the parts of the previous chapter that you didn't fully understand the first time. Take your time, this is tricky stuff.
        -
            link: https://www.brandons.me/blog/why-rust-strings-seem-hard
            title: Why Rust strings seem hard
            info: This article explains why it can be more cumbersome to work with strings in Rust than it is in Python, and how this makes sense.

    Struct and impl:
        -
            link: https://doc.rust-lang.org/stable/book/ch05-00-structs.html
            title: The Rust Book - Using Structs to Structure Related Data
            info: Struct and impl are Rust's alternatives to class and method. This should be relatively easy to grasp, at least compared to the borrowing stuff.
    
    Assignment:
        -
            title: Image viewer
            ^merge: feature
            text: |
                Translate your image decoder/viewer from the *Gates, bits, bytes, numbers* assignment to Rust!

                A small part of the work has already been provided in the template. 

                When you get your viewer to work, compile and run the program in release mode. How does it compare to your Python implementation in terms of performance?
