# Goals

## Main goal
[describe the goal]


## Tasks
[list the tasks]
- ...

# Example Mapping
For the top 3 tasks work out the rules and examples

### Story: TODO

**Rule 1:**  TODO

- **Example 1.1:** TODO
    1. ...


## Questions
[list your questions here]


# Domain model

```plantuml
' Configuration stuff (you should not change this)
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho


entity "Entity" as Entity {
  *attribute: text
}

note left of Entity
  This is an example. Please change it ;)
end note



```

# User flows
For the top 3 tasks you have defined above you should create a user flow (so one user flow per task)


```plantuml
' Some settings to make your flow chart look better
hide circle
skinparam linetype ortho

start
#lightblue:name of screen;
#cyan:action;
if (decision?) then
  #lightblue:error screen;
  detach
endif
#lightblue:success screen;
#cyan:do something (action);
stop
```
