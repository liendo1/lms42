name: Flask SQL
description: Add a database, multi-user support and game review to Lingo.
resources:
    -
        link: https://cs50.harvard.edu/x/2023/weeks/9/
        title: CS50 - Flask
        info: Watch the section on sessions starting at 1:44:36 tot 1:55:48
    -
        link: https://flask-session.readthedocs.io/en/latest/
        title: Flask-Session - Official documentation
    - 
        link: https://s3.us-east-2.amazonaws.com/prettyprinted/flask_cheatsheet.pdf
        title: Pretty Printed - Flask Cheat Sheet
        info: The same single A4 cheat sheet we saw earlier. Also includes the use of sessions.
    -
        link: https://uniwebsidad.com/libros/explore-flask/chapter-8/creating-macros
        title: Make macro's in your jinja for not repeating yourself.
        info: Implementing macro's so that you can implement like 'components' in your template.

goals:
    flask: 1
    python-sql: 1
    auth: 1

assignment:
    - |
        Today's assignment builds upon your submission for the *Flask basics* assignment: Lingo! You'll be adding a PostgreSQL database (using *postgresqlite*), multi-user support (using *sessions*) and a way to review all of the guesses for a game that was played earlier.

        Start by retrieving the provided template (which contains just one file: `schema.sql`), and copying your solution to the *Flask basics* assignment into this directory.

    -
        title: Database
        text: |
            Implement the exact requirements from the *Flask basics* assignment, but this time using a database to store all data. The database schema has been provided in `schema.sql`. You can initialize a database with this schema and explore it using:

            ```sh
            poetry add postgresqlite pgcli
            poetry run postgresqlite < schema.sql
            poetry run postgresqlite pgcli
            ```

            **Hints:**

            - Start by creating a database connection in `app.py`, like you did in the previous assignment.
            - All use of `global` variables in your original implementation should be replaced by database queries.
            - We recommend that you start by ignoring the `players` table completely. When inserting rows in the `games` table, you can just use `player_id` `1`.
            - The `result` field of the `games` table can be `null` (the game is not finished yet), a positive integer (the number of guesses after which the game was won) or `-1` (game lost, after 15 incorrect guesses).
        weight: 3
        ^merge: feature

    -
        title: Multi-user support
        text: |
            Let's add multi-user support! Here are some steps you can follow:

            - Create a login/registration page.
                - It should consist of a form containing a player name field, a password field and a submit button.
                - Upon submission, the form data should be POSTed.
                - Your app should check if the player name already exists in the database.
                    - If so, check that the provided password matches the password stored with the existing player in the database. If it doesn't match, the login page should be shown again, displaying a *wrong password* message. If it does match, the player should be logged in by setting the *player id* in the session, and then redirect to the *Play game* page.
                    - If not, a new player with the provided name and password should be inserted into the database. Then this player should be logged in and redirect, like above.
            - Redirect browsers that try to interact with the *Play game* page but are not logged in (have no *player id* in the session) to the login page.
            - Add a logout link to the header of the page, next to *View history* link. It should only be displayed when the player is logged in. Clicking it should log the player out, and redirect to the login page.

        weight: 3
        ^merge: feature

    - |
        <div class="notification is-important">
            Storing plain passwords (in a database) is <i>not</i> something you should ever do after today. We'll learn why not and what to do instead in the next assignment.
        </div>

    -
        title: Extend *View history*
        text: |
            Extend the *View history* page:

            - List the name of the player for each of the rows that is shown.
            - Make each secret word in the rows a clickable link to something like `/games/123` where `123` would be the id of the game.
            - Implement a route for `/games/...`. It should allow the user to view all guesses that the player made during the game. You can reuse the *Play game* template for this, but you should of course make sure that the input field is not shown.
        weight: 2
        ^merge: feature
