from common import grade_attempt


def test_start_pair_programming(client, db_session):
    client.login("student1")
    start_and_submit_attempt(client, "linux")

    client.login("student2")
    start_and_submit_attempt(client, "linux")
    start_and_submit_attempt(client, "html")

    client.login("teacher1")
    grade_attempt(client, 'linux', 'student1')
    grade_attempt(client, 'linux', 'student2')

    client.login("student4")
    start_and_submit_attempt(client, "linux")
    start_and_submit_attempt(client, "html")
    client.open("/curriculum/css")
    client.find("a", text="pair programming").require(1)
    client.find("a.pair_option", href="/people/student1").require(0)
    client.find("a.pair_option", href="/people/student2").require(1)
    client.find("a.pair_option", href="/people/student3").require(0)
    client.find("a.pair_option", href="/people/student4").require(0)


def start_and_submit_attempt(client, node_id):
    client.open(f"/curriculum/{node_id}")
    (client.find("input", value='Start attempt') + client.find("input", value='Start attempt anyway')).submit()
    client.find("input", value='Submit attempt').submit(finished="yes")
    client.find("input", value="Yes, I'm sure").submit()
