from ..app import db
import sqlalchemy as sa


class Company(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=False)
    logo = sa.Column(sa.String)
    size = sa.Column(sa.String)
    login = sa.Column(sa.String, nullable=False)
    location = sa.Column(sa.String)
    description = sa.Column(sa.String)
    promotext = sa.Column(sa.String)
    intern_fee = sa.Column(sa.Integer)
    misc = sa.Column(sa.String)
    is_visible = sa.Column(sa.Boolean, default=False)


    # Loading related teams via a LEFT OUTER JOIN (lazy='joined') each
    # time a Company is loaded, solving the N+1 problem.
    teams = sa.orm.relationship(
        'Team',
        backref='company',
        lazy='joined',
        cascade="all, delete-orphan",
        )

    @property
    def logo_file(self):
        if self.logo:
            return f"/static/logos/{self.logo}"


class Team(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=False)
    availability = sa.Column(sa.Integer, nullable=False, default=0)
    size = sa.Column(sa.String, nullable=False)
    location = sa.Column(sa.String, nullable=False)
    technologies = sa.Column(sa.String, nullable=False)
    working_on = sa.Column(sa.String)
    misc = sa.Column(sa.String)
    contact = sa.Column(sa.String, nullable=False)
    dutch_only = sa.Column(sa.Boolean, default=False)

    company_id = sa.Column(sa.Integer, sa.ForeignKey('company.id'))