from ..app import db, app
import flask
from flask_login import login_required, current_user


@app.route('/', methods=['GET'])
def frontpage():
    return flask.render_template('frontpage.html')


@app.route('/coc', methods=['GET'])
def coc():
    version = flask.request.args.get('version')
    return flask.render_template(f'coc{"-"+version if version else ""}.html')

@app.route('/happy-coding', methods=['GET'])
def happy_coding():
    return flask.render_template('happy-coding.html')

@app.route('/python-cheat-sheet', methods=['GET'])
def python_cheat_sheet():
    with open('lms42/templates/python-cheat-sheet.md') as file:
        markdown = file.read()
    return flask.render_template('markdown.html', title="Python cheat sheet", body=markdown)
